package maze

from std import unittest.testmacro.Test
from std import unittest.testmacro.TestCase

public class Queue<T> {
    var max_length: Int = 128

    var arr: Array<Option<T>>

    var front: Int = 0

    var rear: Int = 0

    init() {
        this.arr = Array<Option<T>>(this.max_length, item: None)
    }

    init(maxLength: Int) {
        this.max_length = maxLength
        this.arr = Array<Option<T>>(maxLength, item: None)
    }

    public func add(value: T): Bool {
        if (this.isFull()) {
            return false
        }
        this.arr[this.rear] = value
        this.rear = (this.rear + 1) % (this.max_length + 1)
        return true
    }

    public func leave(): Option<T> {
        if (this.isEmpty()) {
            return None
        }
        let res = this.arr[this.front]
        this.front = (this.front + 1) % (this.max_length + 1)
        return res
    }

    public func isFull(): Bool {
        return (this.front - this.rear == 1 || (this.front == 0 && this.rear == this.max_length));
    }

    public func isEmpty(): Bool {
        return this.front == this.rear;
    }
}
